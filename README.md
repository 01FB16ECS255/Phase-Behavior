perf tool is used on linux to determine total instructions,cache-references,cache-misses,branch-misses

run on terminal

for other application you want to try replace ls by <other application> 
ls:perf stat -o ls.txt -e instructions,cache-references,cache-misses,branch-misses -I 10 ls 
-------------
-o : to give output file in which these values are going to store
-e:events list we want to find,
-I:to mention about milliseconds in our command we took for 10 ms because value should be >=10ms,in str.c file we are dividing those values by 10 so that particular values we be to 1ms
-------------

perf stat -o ls.txt --append -e instructions,cache-references,cache-misses,branch-misses -I 10 ls 
    :append values to ls.txt
    
-------------------------------------------------------------------------------------------------------------------------------------

cosinesimmilarity using hadoop
CosineSimilarity.java: our java file name

su hduser
start-all.sh


i) Compile
(cd <path to the folder containing the java file>)
cd /home/hduser/Desktop/WordCountF/

javac -classpath /usr/local/hadoop/share/hadoop/common/hadoop-common-2.7.2.jar:/usr/local/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-client-core-2.7.2.jar:/usr/local/hadoop/share/hadoop/common/lib/commons-cli-1.2.jar -d /home/hduser/Desktop/WordCountF *.java

Move manually all the .class files that are generated in /home/hduser/Desktop/WordCountF/ to a new folder, say WordCountC => /home/hduser/Desktop/WordCountF/WordCountC

ii) Create a .jar file
(cd <path to the folder containing the java file>)
cd /home/hduser/Desktop/WordCountF/

jar -cvf WordCountJ.jar -C /home/hduser/Desktop/WordCountF/WordCountC .

iii) Load the input files onto Hadoop
(hadoop fs -put <full path of the file/dir on the local machine> <full path of storage on Hadoop>)
hadoop fs -put /home/hduser/Desktop/WordCountF/ls1.txt /Input/

iv) Run the .jar file
cd /usr/local/hadoop

(bin/hadoop jar <full path of the jar file created> <Java filename> <full path f the input folder on hadoop> <output folder on hadoop>)
bin/hadoop jar /home/hduser/Desktop/WordCountF/WordCountJ.jar CosineSimilarity /Input Output

Navigate to "http://localhost:50070" on your web browser
Click on the Utilities tab (top right) => Browse the file system
Click on user => hduser => Output => part-r-00000 => Download
The downloaded file contains the required output
____________________________________________________________________________________________________________________________________________


Plotting of graph:

We have used bar graph to plot different execution phases using excel

_____________________________________________________________________________________________________________________________________

For K-means we have used Anaconda(Python Distribution)

k_means.ipynb
_____________________________________________________________________________________________________________________________________

